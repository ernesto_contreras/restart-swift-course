//
//  ImageExtension.swift
//  Restart
//
//  Created by Ernesto Jose Contreras Lopez on 18/8/22.
//

import SwiftUI

extension Image {
    func makeResizable() -> some View {
        self
            .resizable()
            .scaledToFit()
    }
}
