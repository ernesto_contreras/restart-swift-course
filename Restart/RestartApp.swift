//
//  RestartApp.swift
//  Restart
//
//  Created by Ernesto Jose Contreras Lopez on 17/8/22.
//

import SwiftUI

@main
struct RestartApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
